/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_COMMON_H_
	#define KENNO_COMMON_H_

	#include "Pool.hpp"

	#include <cstdint>

	namespace kenno
	{
		using Priority        = std::int16_t;
		
		using IDScene         = std::uint16_t;
		
		using IDTypeEvent     = Pool::ID;
		using IDEvent         = Poolable::ID;
		using IDListener      = Poolable::ID;
		
		using IDEntity        = std::uint64_t;
		using IDTypeComponent = Pool::ID;
		using IDComponent     = Poolable::ID;
		
		using TagsEntity      = std::uint32_t;
		
		struct Userdata
		{
			virtual ~Userdata() = default;
		};
	}
#endif
