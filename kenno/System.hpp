/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_SYSTEM_H_
	#define KENNO_SYSTEM_H_

	#include "Pool.hpp"
	#include "Component.hpp"

	#include <type_traits>

	namespace kenno
	{
		class World;
		
		/**
		 * Base class for a system that loops over a primary
		 * type of component as specified by the template
		 * argument and receives a list of such components
		 * to iterate over every frame in a manner implemented
		 * individually for each derived class.
		 *
		 * If additional components are needed, the entity ID
		 * can be retrieved from a component and the world
		 * reference passed into the update method can be used
		 * to query the entity for those additional components.
		 */
		template<typename T>
		class System
		{
			static_assert(std::is_base_of<Component, T>{}, "Component type must inherit component!");

			public:
				using TypeComponent = T;

			public:
				/**
				 * Called every frame by the world on each list
				 * of matching components in every scene.
				 *
				 * @param world      A reference to the parent world.
				 * @param components A struct containing the list of components as a pointer and the count.
				 * @param dt         The delta time in seconds since the last frame.
				 */
				void update
				(
					World &world,
					Pool::ViewSubpool<T> &components,
					const float dt,
					const float dtFixed,
					const float timestep
				)
				{
					performUpdate(world, components, dt, dtFixed, timestep);
				}
			
				void updateWithoutComponents
				(
					World &world,
					const float dt,
					const float dtFixed,
					const float timestep
				)
				{
					performUpdateWithoutComponents(world, dt, dtFixed, timestep);
				}
			
				virtual bool updatesComponents() const noexcept { return true; }

				virtual ~System() = default;

			protected:
				/**
				 * Overridden to provide the actual implementation
				 * of the component iteration of the system.
				 * See update().
				 */
				virtual void performUpdate
				(
					World &world,
					Pool::ViewSubpool<T> &components,
					const float dt,
					const float dtFixed,
					const float timestep
				) = 0;
			
				virtual void performUpdateWithoutComponents
				(
					World &world,
					const float dt,
					const float dtFixed,
					const float timestep
				) = 0;
		};
		
		class SystemWithoutComponents : public System<Component>
		{
			public:
				bool updatesComponents() const noexcept override { return false; }
		};
	}
#endif
