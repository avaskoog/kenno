/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_RESULT_H_
	#define KENNO_RESULT_H_

	#include <string>

	namespace kenno
	{
		/**
		 * Used to provide a success state alongside a return value.
		 * In case of failure, the error string will be filled in.
		 */
		template<typename T>
		struct Result
		{
			Result() = default;
			Result(Result<T> &&) = default;

			T value;
			const bool success;
			const std::string error;
		};
		
		/**
		 * Used to provide a success state without a return value.
		 * In case of failure, the error string will be filled in.
		 */
		struct Status
		{
			Status() = default;
			Status(Status &&) = default;

			const bool success;
			const std::string error;
		};
	}
#endif
