/**
 * @author		Ava Skoog
 * @date		2018-10-06
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_EVENT_H_
	#define KENNO_EVENT_H_

	#include "common.hpp"

	namespace kenno
	{
		/**
		 * Base class for custom event types.
		 */
		class Event : public Poolable
		{
			public:
				Event
				(
					const IDEvent &identifier,
					const IDEntity &entity,
					const IDTypeComponent &type,
					const IDComponent &component
				)
				:
				Poolable   {identifier},
				m_entity   {entity},
				m_type     {type},
				m_component{component}
				{
				}

				Event() = delete;
				Event(Event &&) = default;
				Event(const Event &) = default;
				Event &operator=(Event &&) = default;
				Event &operator=(const Event &) = default;
			
				IDEntity entity() const noexcept { return m_entity; }
				IDTypeComponent typeComponent() const noexcept { return m_type; }
				IDComponent component() const noexcept { return m_component; }
			
				virtual ~Event() = default;
			
			private:
				IDEntity m_entity;
				IDTypeComponent m_type;
				IDComponent m_component;
		};
		
		/**
		 * Built-in event type for the creation of a component.
		 */
		class EComponentCreated : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for the destruction of a component.
		 */
		class EComponentDestroyed : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for the creation of an entity.
		 */
		class EEntityCreated : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for the destruction of an entity.
		 */
		class EEntityDestroyed : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for a component becoming active.
		 */
		class EComponentActive : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for a component becoming inactive.
		 */
		class EComponentInactive : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for an entity becoming active.
		 */
		class EEntityActive : public Event { public: using Event::Event; };
		
		/**
		 * Built-in event type for an entity becoming inactive.
		 */
		class EEntityInactive : public Event { public: using Event::Event; };
	}
#endif
