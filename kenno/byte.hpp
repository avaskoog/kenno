/**
 * @author		Ava Skoog
 * @date		2018-09-23
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_BYTE_H_
	#define KENNO_BYTE_H_

	#include <cstdint>

	namespace kenno
	{
		using Byte = std::uint8_t;
	}
#endif
