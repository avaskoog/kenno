/**
 * @author		Ava Skoog
 * @date		2018-09-22
 * @copyright	2018-2021 Ava Skoog
 */

#ifndef KENNO_COMPONENT_H_
	#define KENNO_COMPONENT_H_

	#include "Pool.hpp"
	#include "common.hpp"

	namespace kenno
	{
		/**
		 * Base class for custom component types.
		 */
		class Component : public Poolable
		{
			public:
				Component
				(
					IDComponent     const identifier,
					IDTypeComponent const typeComponent,
					IDEntity        const entity
				)
				:
				Poolable       {identifier},
				m_typeComponent{typeComponent},
				m_entity       {entity}
				{
				}

				IDTypeComponent typeComponent() const noexcept { return m_typeComponent; }
				IDEntity        entity()        const noexcept { return m_entity; }
			
			private:
				IDTypeComponent m_typeComponent;
				IDEntity        m_entity;
		};
	}
#endif
