#include "World.hpp"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <thread>

class Clock
{
	public:
		Clock(const char *const action);
		void tell();

	private:
		const std::chrono::steady_clock::time_point start;
		const std::string action;
};

class Transform : public kenno::Component
{
	public:
		using kenno::Component::Component;
		float x{33.0f}, y{33.0f}, z{33.0f};
		float xLocal{33.0f}, yLocal{33.0f}, zLocal{33.0f};
};

class System1 : public kenno::System<Transform>
{
	public:
		using kenno::System<Transform>::System;
	
	protected:
		void performUpdate
		(
			kenno::World &world,
			kenno::Pool::ViewSubpool<Transform> &components,
			const float &dt
		) override
		{
			for (std::size_t i{0}; i < components.count; ++ i)
			{
				auto c(components.data + i);

				c->x = c->xLocal;
				c->y = c->yLocal;
				c->z = c->zLocal;

				std::size_t counter{0};
				kenno::IDEntity prev{c->entity()};
				world.forEachParentOfEntity
				(
					c->entity(),
					[&prev, &counter, &c, &world](const kenno::IDEntity &ID)
					{
						if (auto s = world.componentInEntity<Transform>(ID))
						{
							std::cout << "found lvl " << counter << " parent " << ID << " of " << prev << '\n';
							c->x += s->xLocal;
							c->y += s->yLocal;
							c->z += s->zLocal;
							prev = ID;
							++ counter;
						}
						return true;
					}
				);

				std::cout
					<< "System: transform "
					<< static_cast<std::int64_t>(c->entity())
					<< ", x = " << c->x
					<< ", y = " << c->y
					<< ", z = " << c->z
					<< '\n';
			}
		}
};

int main()
{
	using namespace std::chrono_literals;

	constexpr std::size_t count{8};
	
	Clock c1{"create world"};
	kenno::World world{[]()
	{
		static kenno::IDComponent ID{count * 4};
		return (ID ++);
	}};
	c1.tell();
	
	std::cout << '\n';
	
	{
		Clock c{"create 'System1' system"};
		auto s(world.createSystemWithPriorityAndArgs<System1>(0));
		if (!s.success)
		{
			std::cerr << "ERROR: " << s.error << '\n';
			return -1;
		}
		c.tell();
	}
	
	std::cout << '\n';
	
	{
		std::stringstream ss;
		ss << "register 'Transform' with " << count << " capacity";
		Clock c{ss.str().c_str()};
		auto s(world.registerTypeOfComponentWithCountsAndGrowability<Transform>(0, "Transform", 1, count, true));
		if (!s.success)
		{
			std::cerr << "ERROR: " << s.error << '\n';
			return -1;
		}
		c.tell();
	}
	
	std::cout << '\n';
	
	{
		Clock c{"create scene with ID 0"};
		auto s(world.createScene(0));
		if (!s.success)
		{
			std::cerr << "ERROR: " << s.error << '\n';
			return -1;
		}
		c.tell();
	}
	
	std::cout << '\n';
	
	{
		std::stringstream ss;
		ss << "create " << count << " entities in scene 0";
		Clock c{ss.str().c_str()};
		
		for (std::size_t i{0}; i < count; ++ i)
		{
			auto s(world.createEntityInScene(static_cast<kenno::IDEntity>(i), 0));
			if (!s.success)
			{
				std::cerr << "ERROR (entity " << i << "): " << s.error << '\n';
				return -1;
			}
		}
		
		c.tell();
	}
	
	std::cout << '\n';
	
	{
		Clock c{"set parent-child relationships"};
		
		if (!world.entityParentOf(0, 1))
		{
			std::cerr << "ERROR setting parent-child relationship 0 1\n";
			return -1;
		}
		world.printParChi();
	
		if (!world.entityParentOf(1, 2))
		{
			std::cerr << "ERROR setting parent-child relationship 1 2\n";
			return -1;
		}
		world.printParChi();
		
		if (!world.entityParentOf(2, 3))
		{
			std::cerr << "ERROR setting parent-child relationship 1 2\n";
			return -1;
		}
		world.printParChi();
		
		c.tell();
	}
	
	std::cout << '\n';
	
	{
		Clock c{"move parent-child relationship 0 1 > 3 1"};
		if (!world.entityParentOf(3, 1))
		{
			std::cerr << "ERROR moving parent-child relationship 0 1 > 3 1\n";
			return -1;
		}
		world.printParChi();
		c.tell();
	}
	
	{
		auto s(world.createEntityInScene(0, 1));
		if (!s.success)
			std::cout << "Trying on purpose to create entity in an invalid scene...\nERROR: " << s.error << '\n';
	}
	
	std::cout << '\n';
	
	{
		Clock c{"create 'Transform' for half the entities"};
		
		for (std::size_t i{0}; i < count / 2; ++ i)
		{
			auto s(world.createComponentInEntity<Transform>(static_cast<kenno::IDComponent>(i), static_cast<kenno::IDEntity>(i)));
			if (!s.success)
			{
				std::cerr << "ERROR: " << s.error << '\n';
				return -1;
			}
			
			s.value->x = 1.0f;
			s.value->y = 1.0f;
			s.value->z = 1.0f;
		}

		c.tell();
	}
	
	std::cout << '\n';
	
	{
		auto s(world.createComponentInEntity<Transform>(0, count * 10));
		if (!s.success)
			std::cout << "Trying on purpose to create a component in an invalid entity...\nERROR: " << s.error << '\n';
	}
	
	std::cout << '\n';
	
	{
		Clock c{"access 'Transform' in entity 0"};
		auto s(world.componentInEntity<Transform>(0));
		if (!s)
		{
			std::cerr << "ERROR accessing 'Transform' in entity 0\n";
			return -1;
		}
		c.tell();
		
		std::cout << "Transform x = " << s->x << ", y = " << s->y << ", z = " << s->z << '\n';
	}
	
	std::cout << '\n';
	
	{
		auto s(world.componentInEntity<Transform>(count * 10));
		std::cout << "Trying on purpose to access a component in an invalid entity...\nRETURNED: " << s << '\n';
	}
	
	std::cout << '\n';
	
	{
		Clock c{"create scene with ID 1"};
		auto s(world.createScene(1));
		if (!s.success)
		{
			std::cerr << "ERROR: " << s.error << '\n';
			return -1;
		}
		c.tell();
	}

	std::cout << "\nStarting the main loop on the main thread...\n\n";
	std::cout << "Starting async scene manip on other thread...\n\n" << std::flush;
	
	world.manipulateSceneAsync(1, [](kenno::World &world, const kenno::IDScene &scene)
	{
		for (std::size_t i{0}; i < count; ++ i)
		{
			const std::size_t ID{i + count};
			auto s(world.createEntityInScene(static_cast<kenno::IDEntity>(ID), scene));
			if (!s.success)
			{
				std::cerr << "ERROR (entity " << ID << "): " << s.error << '\n';
				return false;
			}
			
			std::cout << "subscene thread: added entity " << (i + 1) << " / " << count << " (ID " << ID << ")\n";

			{
				auto s(world.createComponentInEntity<Transform>(static_cast<kenno::IDComponent>(i), static_cast<kenno::IDEntity>(ID)));
				
				if (!s.success)
				{
					std::cerr << "ERROR: " << s.error << '\n';
					return false;
				}
				else
				{
					s.value->x = 2.0f;
					s.value->y = 2.0f;
					s.value->z = 2.0f;
				}
				
				if (i % 2)
				{
					auto s(world.destroyComponentInEntity(static_cast<kenno::IDComponent>(i), static_cast<kenno::IDEntity>(ID)));
					
					if (!s.success)
					{
						std::cerr << "ERROR: " << s.error << '\n';
						return false;
					}
					else
						std::cout << "subscene thread: removed component " << i << " from entity " << ID << '\n';
				}
			}
			
			{
				if (i == count / 2)
				{
					auto s(world.destroyEntity(static_cast<kenno::IDEntity>(ID)));
					if (!s.success)
					{
						std::cerr << "ERROR: " << s.error << '\n';
						return false;
					}
					else
						std::cout << "subscene thread: removed entity " << ID << '\n';
				}
			}
			
			std::this_thread::sleep_for(4ms);
		}
		
		std::cout << "subscene thread: done adding " << count << " entities!\n";
		return true;
	});
	
	std::size_t f{0};
	int counter{0};
	while (true)
	{
		std::cout << "main thread frame " << (f ++) << '\n';
		world.update(0.0f);
		
		if (world.sceneLoaded(1) && (++ counter) == 2)
		{
			std::cout << "main thread: scene load with err? = " << world.sceneLoadedWithError(1) << '\n';
			std::cout << "main thread: destroying other scene after 2 frames\n";
			world.destroyScene(1);
		}
		
		std::this_thread::sleep_for(10ms);
	}

	return 0;
}

Clock::Clock(const char *const action)
:
start {std::chrono::steady_clock::now()},
action{action}
{
}

void Clock::tell()
{
	auto diff(std::chrono::steady_clock::now() - start);
	auto ms(std::chrono::duration_cast<std::chrono::nanoseconds>(diff));
	std::cout << std::fixed << "Time to " << action << ": " << (static_cast<double>(ms.count()) / 1000000000.0) << "s\n";
}
